"""App settings
------------

This module defines the settings options for the
``{{ cookiecutter.project_slug }}`` app.
"""


from __future__ import annotations

from django.conf import settings  # noqa: F401

{%- if cookiecutter.include_channels == "yes" %}
#: Base route for the websocket routing of the
#: ``{{ cookiecutter.project_slug }}`` app
{{cookiecutter.package_folder | upper }}_WEBSOCKET_URL_ROUTE = getattr(
    settings,
    "{{cookiecutter.package_folder | upper }}_WEBSOCKET_URL_ROUTE",
    "ws/"
)

{%- endif %}