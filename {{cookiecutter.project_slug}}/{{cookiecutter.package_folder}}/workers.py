{%- if cookiecutter.__include_channel_workers == "yes" %}
"""
Websocket routing configuration for the {{ cookiecutter.project_slug }} app.

It exposes the `workers` dictionary, a mapping from worker name
consumer to be used by channels `ChannelNameRouter`.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/worker.html#receiving-and-consumers
"""
from typing import Dict, Any

from {{ cookiecutter.package_folder }} import consumers  # noqa: F401


workers: Dict[str, Any] = {}
{% endif %}