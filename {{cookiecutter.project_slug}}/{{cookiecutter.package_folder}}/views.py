"""Views
-----

Views of the {{ cookiecutter.project_slug }} app to be imported via the url
config (see :mod:`{{ cookiecutter.package_folder }}.urls`).
"""

from __future__ import annotations

from django.views import generic  # noqa: F401

from {{ cookiecutter.package_folder }} import app_settings  # noqa: F401
from {{ cookiecutter.package_folder }} import models  # noqa: F401
