"""URL config
----------

URL patterns of the {{ cookiecutter.project_slug }} to be included via::

    from django.urls import include, path

    urlpatters = [
        path(
            "{{ cookiecutter.project_slug }}",
            include("{{ cookiecutter.package_folder }}.urls"),
        ),
    ]
"""
from __future__ import annotations

from typing import Any

from django.urls import path  # noqa: F401
from django.conf.urls.static import static
from django.conf import settings

from {{ cookiecutter.package_folder }} import views  # noqa: F401

#: App name for the {{ cookiecutter.project_slug }} to be used in calls to
#: :func:`django.urls.reverse`
app_name = "{{ cookiecutter.package_folder }}"

#: urlpatterns for {{ cookiecutter.project_slug }}
urlpatterns: list[Any] = [
    # add urls patterns for {{ cookiecutter.project_slug }}
]

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
