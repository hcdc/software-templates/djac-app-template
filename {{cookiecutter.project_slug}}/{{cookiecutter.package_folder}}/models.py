"""Models
------

Models for the {{ cookiecutter.project_slug }} app.
"""

from __future__ import annotations

from django.db import models  # noqa: F401

from {{ cookiecutter.package_folder }} import app_settings  # noqa: F401
