"""Admin interfaces
----------------

This module defines the {{ cookiecutter.project_slug }}
Admin interfaces.
"""


from django.contrib import admin  # noqa: F401

from {{ cookiecutter.package_folder }} import models  # noqa: F401
