"""App config
----------

App config for the {{ cookiecutter.package_folder }} app.
"""

from django.apps import AppConfig


class DjangoHelmholtzAaiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "{{ cookiecutter.package_folder }}"
