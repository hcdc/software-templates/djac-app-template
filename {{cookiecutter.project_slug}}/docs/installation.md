(installation)=
# Installation

```{warning}

This page has been automatically generated as has not yet been reviewed
by the authors of {{ cookiecutter.project_slug }}!
```


To install the `{{ cookiecutter.project_slug }}` package for your Django
project, you need to follow two steps:

1. {ref}`Install the package <install-package>`
2. {ref}`Add the app to your Django project <install-django-app>`


(install-package)=

## Installation from PyPi

The recommended way to install this package is via pip and PyPi via:

```bash
pip install {{ cookiecutter.project_slug }}
```

or, if you are using `pipenv`, via:

```bash
pipenv install {{ cookiecutter.project_slug }}
```

Or install it directly from
[the source code repository on Gitlab][source code repository] via:

```bash
pip install git+https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git
```

The latter should however only be done if you want to access the
development versions  (see {ref}`install-develop`).

(install-django-app)=

## Install the Django App for your project

To use the `{{ cookiecutter.project_slug }}` package in your Django project,
you need to add the app to your `INSTALLED_APPS`, configure your `urls.py`, run
the migration, add a login button in your templates. Here are the step-by-step
instructions:

1. Add the `{{ cookiecutter.package_folder }}` app to your `INSTALLED_APPS`
2. in your projects urlconf (see {setting}`ROOT_URLCONF`), add include
   {mod}`{{ cookiecutter.package_folder }}.urls` via::

   ```python
    from django.urls import include, path

    urlpatterns += [
        path("{{ cookiecutter.project_slug }}/", include("{{ cookiecutter.package_folder }}.urls")),
    ]
    ```
3. Run ``python manage.py migrate`` to add models to your database
4. Configure the app to your needs (see {ref}`configuration`) (you can also
   have a look into the `testproject` folder in the
   [source code repository][source code repository] for a possible
   configuration).

   Please make sure that you have an `asgi.py` file as in the `testproject` and
   set the `ASGI_APPLICATION` variable in your `settings.py`. See
   https://channels.readthedocs.io/en/stable/installation.html

If you need a deployment-ready django setup for an app like this, please
have a look at the following template:

https://codebase.helmholtz.cloud/hcdc/software-templates/djac-docker-template

That's it!

[source code repository]: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}

(install-develop)=
## Installation for development

Please head over to our
`contributing guide <contributing>`{.interpreted-text role="ref"} for
installation instruction for development.
