"""
Websocket routing configuration for the {{ cookiecutter.project_slug }} project.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""

from typing import List, Any

from channels.routing import URLRouter  # noqa: F401
from django.urls import path  # noqa: F401
import academic_community.routing
{%- if cookiecutter.include_channels == "yes" %}
import {{ cookiecutter.package_folder }}.routing
{%- endif %}

websocket_urlpatterns: List[Any] = (
    academic_community.routing.websocket_urlpatterns + 
    {%- if cookiecutter.include_channels == "yes" %}
    {{ cookiecutter.package_folder }}.routing.websocket_urlpatterns +
    {%- endif %}
    [
        # django project specific routes
    ]
)