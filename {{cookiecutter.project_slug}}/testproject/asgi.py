"""
ASGI config for the {{ cookiecutter.project_slug }} project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

# isort: off
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "testproject.settings")
# isort: on

from django.core.asgi import get_asgi_application

from channels.routing import ProtocolTypeRouter
from channels.auth import AuthMiddlewareStack
from channels.routing import (
    ChannelNameRouter,
    ProtocolTypeRouter,
    URLRouter,
)
from channels.sessions import SessionMiddlewareStack

from channels.security.websocket import AllowedHostsOriginValidator

from .routing import websocket_urlpatterns
from .workers import workers

application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": AllowedHostsOriginValidator(
            SessionMiddlewareStack(
                AuthMiddlewareStack(
                    URLRouter(websocket_urlpatterns)
                )
            )
        ),
        "channel": ChannelNameRouter(workers),
    }
)
