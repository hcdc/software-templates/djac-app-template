"""
Websocket routing configuration for the {{ cookiecutter.project_slug }} project.

It exposes the `workers` dictionary, a mapping from worker name
consumer to be used by channels `ChannelNameRouter`.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/worker.html#receiving-and-consumers
"""
from typing import Dict, Any
import academic_community.workers
{%- if cookiecutter.__include_channel_workers == "yes" %}
import {{ cookiecutter.package_folder }}.workers
{%- endif %}

workers: Dict[str, Any] = {}

{%- if cookiecutter.__include_channel_workers == "yes" %}
workers.update({{ cookiecutter.package_folder }}.workers.workers)
{% endif %}
workers.update(academic_community.workers.workers)
