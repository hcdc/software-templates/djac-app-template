# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "with reuse" {
    create_template '{use_reuse: "yes"}'
    assert [ -e "${PROJECT_FOLDER}/LICENSES/EUPL-1.2.txt" ]
}



@test "without reuse" {
    create_template '{use_reuse: "no"}'
    assert [ ! -e "${PROJECT_FOLDER}/LICENSES/EUPL-1.2.txt" ]
}
