# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test running the test suite" {
    create_template
    setup_venv
    make -C ${PROJECT_FOLDER} test
}

@test "test running the test suite (pipenv)" {
    create_template '{ci_matrix: "pipenv"}'
    setup_venv
    pip install pipenv
    SCENARIO=default
    export PIPENV_PIPFILE=${PROJECT_FOLDER}/ci/matrix/default/Pipfile
    pipenv install
    make -C ${PROJECT_FOLDER} pipenv-test
}

@test "test running the test suite (pytest-xdist)" {
    create_template '{use_pytest_xdist: "yes"}'
    setup_venv
    NPROCS=2 make -C ${PROJECT_FOLDER} test
}
