# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test creating the database" {
    create_template
    setup_venv
    make -C ${PROJECT_FOLDER} database
}

@test "test channels setup" {
    create_template '{include_channels: "yes"}'
    assert [ -e "${PROJECT_FOLDER}/my_python_package/consumers.py" ]
    assert [ ! -e "${PROJECT_FOLDER}/my_python_package/workers.py" ]
    setup_venv
    make -C ${PROJECT_FOLDER} database
}

@test "test channels setup with workers" {
    create_template '{include_channels: "yes", include_channel_workers: "yes"}'
    assert [ -e "${PROJECT_FOLDER}/testproject/workers.py" ]
    assert [ -e "${PROJECT_FOLDER}/my_python_package/workers.py" ]
    setup_venv
    make -C ${PROJECT_FOLDER} database
}
